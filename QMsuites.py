
# -*- coding: utf-8 -*-
#########################################
#                                       #
# Interface to several QM programs. The #
# idea was to eventually be able to     #
# follow the calculations while they're #
# being run. However, I have not got a  #
# clue yet how to do that efficiently   #
# in a queueing system environment so   #
# any hints are welcome.                #
#                                       #
#########################################

from collections import namedtuple

class QMSuite(object):
    comment_char    = None # line comment character
    mem_str_func    = None # small function to return the string for setting memory requirements
    dft_funcs       = None # dictionary translating common keywords
    basis_sets      = None # shorthands for common basis sets
    basis_func      = None
    use_nosym       = None

    config_dict     = None
    
    def __init__(self): raise NotImplementedError(' Do not use QMSuite class directly!')

    def write_Input(self, filepath = None): raise NotImplementedError('Can not write input for undefined suite!')

    def start_Prog(self): raise NotImplementedError('There is no executable associated!')

    def read_Structures(self): raise NotImplementedError('Not applicable.')

    def read_Energies(self): raise NotImplementedError('Not applicable.')

    def read_Gradients(self): raise NotImplementedError('Not applicable.')

    def read_Hessians(self): raise NotImplementedError('Not applicable.')

class Molpro(QMSuite):

    def __init__(self, prog_path = None):
        self.comment_char   = '!'
        self.basis_sets     = { 'STO-3G': 'sto-3g', 'VTZ': 'cc-pVTZ', 'AVTZ': 'aug-cc-pVTZ', 'VQZ': 'cc-pVQZ',
                                'SVP': 'def2-SVP', 'TZVP': 'def2-TZVP'}
        self.dft_funcs      = { 'BP86': 'b-p', 'B3LYP': 'b3lyp', 'M06L': 'mm06-l'}
        # configuration stuff. I admit that's more calculation dependent, but often you don't want to supply it every time.
        if prog_path == None:
            self.program    = os.join([os.environ['HOME'], 'bin/molpro'])
        else:
            self.program    = prog_path


    def write_Input(self, filepath = None, structure = None, calc = None, job = None):
        assert filepath != None
        assert structure != None
        assert calc != None
        assert job != None

        method_opts = ''
        if 'method_opts' in calc.conf:
            for idx, opt in enumerate(calc.conf['method_opts']):
                if opt == ';' or opt == '\n' or idx == len(calc.conf['method_opts']):
                    scf_opts += opt
                else:
                    scf_opts += ',' + opt
        method_str = '{'
        if self.use_fitting:
            method_str += 'df-'
        if method in self.dft_funcs:
            method_str += 'ks'+scf_opts+'}'
        elif method.upper() == 'HF'
            method_str += 'hf'+scf_opts+'}'
            if method.upper() != 'HF': # do the whole thing again

        opt_str = ''       
        if calc.calc_type == 'OPT':
            opt_str = '{optg'
            if 'opt_opts' in calc.conf:
                for idx, opt in enumerate(calc.conf['opt_opts']):
                    if opt == ';' or opt == '\n' or idx == len(calc.conf['opt_opts']):
                        opt_str += opt
                    else:
                        opt_str += ',' + opt
            opt_str += '}'

        try:
            basis_str = 'basis={0}'.format(self.basis_sets[basis])
        except KeyError, e:
            print >> sys.stderr, 'ERROR: Basis {0} not known. Available/Handled basis sets:'
            for b in self.basis_sets:
                print b
            raise e
        else:
            raise Exception('Some other error.')
        with open(filepath, 'w') as inpfile:
            if 'comment' in kwargs:
                print >> inpfile, self.comment_char+kwargs['comment']
            print >> inpfile, 'memory,{0},m'.format(self.nmemory/8)
            if self.use_nosym:
                print >> inpfile, 'nosym'
            print >> inpfile, basis_str
            print >> inpfile, 'geomtyp={0}'.format(structure.format.lower())
            print >> inpfile, 'geometry={\n{0}\n}'.format(structure.get_String())


            if method in self.dft_funcs: # it's DFT!
                print >> inpfile, 'dfunc,{0}'.format(self.dft_funcs[method])
                if 'method_opts' in calc.conf:
                    print >> inpfile, '{'

    def start_Prog(self): pass