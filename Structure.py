
# -*- coding: utf-8 -*-

import re, os, sys
import scipy as np

class Atom(object):
    def __init__(self, elem = None, values = None, group = None, iscart = True):
        self.element    = elem.upper()
        self.group      = group
        if iscart:
            self.X = values[0]
            self.Y = values[1]
            self.Z = values[2]
        else: # zmat. Expects tuples of (atomid, coord_val) as a list.
            self.zcoords = values

class Molecule(object):
    
    def __init__(self, copyfrom = None):
        self.atoms  = None
        self.natoms = None
        self.format = None
        if copyfrom != None:
            raise NotImplementedError('Sorry, copying structures not implemented yet. Complain to developer.')

    def load_Struct(self, fpath = None, stype = None):
        assert fpath != None

        if stype == None: # determine from extension
            ext = fpath.split('.')[-1].upper()
            if ext == 'XYZ':
                self.format = 'XYZ'
            elif ext == 'ZMAT':
                self.format = 'ZMAT'
            else:
                raise Exception('File type not recognized!')
        else:
            self.format = stype

        if self.format == 'XYZ':
            with open(fpath, 'r') as inpfile:
                next(inpfile)
                self.comment = next(inpfile)
                for line in inpfile:
                    try:
                        fields      = line.split()
                        elem, idx   = re.findall(r'(\D+)(\d*)', fields[0])[0]
                        idx         = int(idx)
                        vals        = fields[1:]
                        self.atoms.append(Atom(elem = elem, values = vals, group = idx, iscart = True))
                    except Exception, e:
                        print >> sys.stderr, 'ERROR: Could not parse structure input line: {0}'.format(line)
                        raise e
                inpfile.close()
                self.natoms = len(self.atoms)
        elif self.format == 'ZMAT':
            raise NotImplementedError('No ZMAT yet, sorry.')
            pass

    def write_Struct(self, outfile = None, wtype = 'XYZ'):
        assert self.format != None
        assert outfile != None

        outstr = self.get_String(wtype = wtype)
        with open(outfile, 'w') as ofile:
            print >> ofile, outstr
            ofile.close()

    def get_flat_Array(self):
        retmat = None
        if self.format == 'XYZ':
            retmat = np.zeros((self.natoms*3,))
            for idx, atom in enumerate(self.atoms):
                retmat[idx*3]   = atom.X
                retmat[idx*3+1] = atom.Y
                retmat[idx*3+2] = atom.Z
        elif self.format == 'ZMAT':
            raise NotImplementedError('As the error says.')
        return retmat

    def get_String(self, wtype = 'XYZ'):
        retstr = ''
        if self.format == 'XYZ':
            if wtype.upper() == 'XYZ':
                retstr += '{0:d}\n'.format(self.natoms)
                retstr += '  {0:s}\n'.format(self.comment)
                for atom in self.atoms:
                    retstr += '{0:s}{1:d} {2:12.8f} {3:12.8f} {4:12.8f}\n'.format(atom.elem, atom.group, atom.X, atom.Y, atom.Z)
            elif wtype.upper() == 'ZMAT':
                raise NotImplementedError('No conversion possible yet.')
        elif self.format == 'ZMAT':
            raise NotImplementedError('No ZMAT yet.')
        return retstr

    def move(self, displ_vec = None, nosave = False): pass

    def rotate(self, final_arr = None, nosave = False): pass
