
# -*- coding: utf-7 -*-

import os

class Job(object):
    
    def __init__(self, struct = None):
        self.structure  = struct
        self.calcs      = []
        self.conf       = { 'memory': 10, 'procs': 1, 'scratch': '/scr/'+os.environ['USER']}

    def add_Calc(self, calc = None):
        assert calc != None
        self.calcs.append(calc)

    def configure(self, **kwargs):
        """ Embarassingly basic configuration function which takes about any important parameter except
        the directives for the calculation itself.
        """
        if 'memory' in kwargs:      self.conf['memory']     = kwargs['memory']
        if 'procs' in kwargs:       self.conf['procs']      = kwargs['procs']
        if 'scratch' in kwargs:     self.conf['scratch']    = kwargs['scratch']

    def run(self): pass

class Optimization(SinglePoint):

    def __init__(self, qmsuite = None): pass

class SinglePoint(object):

    def __init__(self, method = 'B3LYP', basis = 'TZVP', use_df = True, **kwargs):
        """ Constructor function that configures basically everything.
        Handled options:

        method = 'B3LYP'    method string
        basis  = 'TZVP'     main basis set
        use_df = True       use density fitting
        df_basis            basis to use for density fitting
        method_opts         extra options for the supplied method as a list of tuples
        """
        self.calc_type  = 'SP'
        self.conf       = { 'method': method.upper(), 'basis': basis.upper(), 'use_df': use_df, 'nosym': False} # dictionary with options
        self.keywords   = {}
        self.options    = {}
        # handle options
        if use_df:
            if 'df_basis' in kwargs:
                self.conf['df_basis'] = kwargs['df_basis']
            else: # assume corresponding basis
                self.conf['df_basis'] = self.conf['basis']+'-DF'
        if 'nosym' in kwargs: self.conf['nosym'] = kwargs['nosym']
        if 'method_opts' in kwargs:
            for opt, val in kwargs['method_opts']:
                self.options[opt] = val
        if 'method_keys' in kwargs:
            for opt, val in kwargs['method_keys']:
                self.keywords[opt] = val
